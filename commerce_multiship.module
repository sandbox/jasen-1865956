<?php

/**
 * @file
 * Handles main functionality for Commerce multiship module.
 */

/**
 * Implements hook_commerce_shipping_service_info_alter().
 */
function commerce_multiship_commerce_shipping_service_info_alter(&$shipping_services) {
  // Make all of the Purolator services go through this module.
  $purolator_services = array_filter(variable_get('commerce_purolator_services', array()));
  foreach ($purolator_services as $key => $val) {
    $shipping_services[$val]['callbacks']['rate'] = 'commerce_multiship_service_rate_order';
  }
  if (isset($shipping_services['local_shipping'])) {
    $shipping_services['local_shipping']['callbacks']['rate'] = 'commerce_multiship_service_rate_order';
  }
}

/**
 * Shipping service callback: returns a base price array for a shipping service
 * calculated for the given order.
 */
function commerce_multiship_service_rate_order($shipping_service, $order) {
  $shipments = commerce_multiship_get_shipments($order);
  if($shipping_service['shipping_method'] == 'local_shipping'){
    if (commerce_multiship_can_ship_locally($shipments)) {
      $total = commerce_multiship_get_local_shipping_price($shipments);
      return array(
        'amount' => commerce_currency_decimal_to_amount($total, 'CAD'),
        'currency_code' => 'CAD',
        'data' => array(),
      );
    }
    else {
      return FALSE;
    }
  }
  elseif ($shipping_service['shipping_method'] == 'purolator') {
    // First attempt to recover cached shipping rates for the current order.
    $rates = commerce_shipping_rates_cache_get('purolator', $order, variable_get('commerce_purolator_rates_timeout', 0));
    // If no rates were recovered from the cache or the cached rates are over one minute old...
    if (!$rates) {
      $rates = array();
      foreach ($shipments as $key => $shipment) {
        // Build the rate request for the current order. This a rate request
        // object.
        $rate_request_object = commerce_purolator_build_full_rate_request($shipment['address'], $shipment['weight'], $shipment['num_packages']);
        // If we got a valid rate request object back...
        if ($rate_request_object) {
          // Submit the API request to Purolator.
          $response = commerce_purolator_api_request($rate_request_object, t('Requesting shipping rates for Order @order_number', array('@order_number' => $order->order_number)));
          if (!empty($response)) {
            // Parse the response to cache all requested rates for the current
            // order.
            foreach ($response as $service_id => $estimate) {
              // Extract the service name and price information from the rate
              // object.
              $decimal = (string) $estimate->TotalPrice;
              $currency_code = 'CAD';
              // If we've already added this service to the rates array just
              // increment the cost.
              if (isset($rates[$service_id])) {
                $rates[$service_id]['amount'] += commerce_currency_decimal_to_amount($decimal, $currency_code);
              }
              else {
                // Otherwise add an item to the rates array for the current
                // service.
                $rates[$service_id] = array(
                  'amount' => commerce_currency_decimal_to_amount($decimal, $currency_code),
                  'currency_code' => $currency_code,
                  'data' => array(),
                );
              }
            }
            // Cache the calculated rates for subsequent requests.
            commerce_shipping_rates_cache_set('purolator', $order, $rates);
          }
        }
      } // endforeach.
    }
    return isset($rates[$shipping_service['name']]) ? $rates[$shipping_service['name']] : FALSE;
  }
  // Return the rate for the requested service or FALSE if not found.
}
/**
 * Grab an array of unique shipments
 */
function commerce_multiship_get_shipments($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $shipments = array();
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    $line_item = $line_item_wrapper->value();
    // Generate a unique key that will identify the address.
    $key = md5(str_replace(' ', '', strtolower(json_encode($line_item->field_ship_postal_code))));
    // If we haven't encountered this address before add a new destination to
    // our shipments array.
    if (!isset($shipments[$key])) {
      $shipments[$key] = array(
        'address' => $line_item->field_ship_postal_code[LANGUAGE_NONE][0],
        'weight' => commerce_physical_product_line_item_weight($line_item),
        'num_packages' => $line_item_wrapper->quantity->value(),
      );
      // Otherwise just increment the existing shipment's weight and item count.
    }
    else {
      $weight = commerce_physical_product_line_item_weight($line_item);
      $shipments[$key]['weight']['weight'] += $weight['weight'];
      $shipments[$key]['num_packages'] += $line_item_wrapper->quantity->value();
    }
  }
  return $shipments;
}
/**
 * Implements hook_form_alter().
 */
function commerce_multiship_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'commerce_line_item_edit_edit_line_item_form') {
    if (isset($form['field_ship_postal_code'])) {
        $form['#after_build'][] = 'commerce_multiship_line_item_form_after_build';
        $form['field_ship_postal_code']['#element_validate'][] = 'commerce_multiship_shipping_validate';
    }
  }
}

/**
 *
 */
function commerce_multiship_line_item_form_after_build($form, &$form_state){
  if (isset($form['field_ship_postal_code'])) {
    if (isset($form_state['values']['field_ship_postal_code'][LANGUAGE_NONE][0])) {
      $address = $form_state['values']['field_ship_postal_code'][LANGUAGE_NONE][0];
    }
    else {
      $address = field_get_items('commerce_line_item', $form['#entity'], 'field_ship_postal_code');
      $address = $address[0];
    }
    // If this line item has an address associated with it and this order has
    // a valid weight then attach a shipping estimate.
    if (!empty($address['country']) && !empty($address['postal_code']) 
          && ($weight = commerce_physical_product_line_item_weight($form['#entity'])) && $weight['weight']       
        ) {
      $quantity = $form['#entity']->quantity;
      $weight = commerce_physical_product_line_item_weight($form['#entity']);
      $request = commerce_purolator_build_full_rate_request($address, $weight, $quantity);
      $response = commerce_purolator_api_request($request, t('Sshipping estimate to set blackout dates'));
      if (!isset($response['Errors'])) {
        if( array_key_exists('PurolatorExpress', $response) ) {
          $form['estimated_shipping_cost'] = array(
            '#markup' => '<div>Estimated shipping cost: <em>$' . $response['PurolatorExpress']->TotalPrice . '</em></div>',
            '#weight' => 10,
          );
          $shipment = $response['PurolatorExpress'];
          // add each date from now to the expected delivery date to the blackout dates
          // excludes the last date
          $start = new DateTime();
          $end = new DateTime($shipment->ExpectedDeliveryDate);
          $interval = DateInterval::createFromDateString('1 day');
          // add 1 to the last day to include it in the period
          $period = new DatePeriod($start, $interval, $end->modify( '+1 day' ));

          foreach($period as $day) {
            $noship_dates[] = $day->format('Y-m-d');
          }
          
          $entity_id = 'entity_id-' . $form['#entity']->line_item_id;          
          $settings = array("blackoutShipDates" => array( $entity_id => $noship_dates));          
          drupal_add_js($settings, 'setting');  
       }
      }
      else {
        form_set_error('field_ship_postal_code', $response['Errors'][0]->Description);
      }
    }
  }
  return $form;
}


function commerce_multiship_shipping_validate($element, &$form_state, $form){
  //dpm($element);
  //dpm($form_state);
  //$address = field_get_items('commerce_line_item', $form['#entity'], 'field_ship_postal_code');
  //$form_state['rebuild'] = TRUE;
  //$address = $form_state['values']['field_ship_postal_code'][LANGUAGE_NONE][0];
  //if (!empty($address['country']) && !empty($address['postal_code'])) {
  //  $weight = commerce_physical_product_line_item_weight($form['#entity']);
  //  $request = commerce_purolator_build_quick_rate_request($address, $weight);
  //  $response = commerce_purolator_api_request($request, 'quick');
  //  if(!isset($response['Errors'])){
  //    $form['shipping_cost'] = array(
  //      '#markup' => '<div>Estimated shipping cost: ' . $response['PurolatorExpress']->TotalPrice . '</div>',
  //      '#weight' => 10,
  //    );
  //  }
  //  else {
  //    form_set_error('field_ship_postal_code', $response['Errors'][0]->Description);
  //  }
  //}
}
function commerce_multiship_commerce_shipping_service_rate_options_alter(&$options, $order) {
  //dpm($options);
  //dpm($order);
  if (isset($options['local_shipping'])) {
    // Make all of the Purolator services go through this module.
    $purolator_services = array_filter(variable_get('commerce_purolator_services', array()));
    foreach ($purolator_services as $key => $val) {
      unset($options[$val]);
    }
  }
}
/**
 *
 */
function commerce_multiship_can_ship_locally($shipments) {
  $count = 0;
  $num_local = 0;
  foreach ($shipments as $key => $shipment) {
    $count++;
      // We can only provide the local shipping option if all of the items in
      // this order are eligible for local shipping.
    if (!($prices = commerce_local_shipping_get_price($shipment['address'])) || ++$num_local != $count) {
      return FALSE;
    }
  }
  return TRUE;
}
/**
 *
 */
function commerce_multiship_get_local_shipping_price($shipments) {
  $local_prices = array();
  foreach ($shipments as $key => $shipment) {
    $postal_code = strtolower($shipment['address']['postal_code']);
    $prices = commerce_local_shipping_get_price($shipment['address']);
    if (!isset($local_prices[$postal_code])) {
      $local_prices[$postal_code] = array(
        'price' => 0,
        'quantity' => 0,
      );
    }
    $current_quantity = $local_prices[$postal_code]['quantity'];
    for($i = $current_quantity + 1; $i <= $current_quantity + $shipment['num_packages']; $i++){
      if($i == 1){
        $local_prices[$postal_code]['price'] += $prices[0];
      }
      elseif($i > 1 and $i <= 5) {
        $local_prices[$postal_code]['price'] += $prices[1];
      }
      else {
        $local_prices[$postal_code]['price'] += $prices[2];
      }
    }
  } //endforeach
  $total = 0;
  foreach($local_prices as $postal_code => $local_price){
    $total += $local_price['price'];
  }
  return $total;
}
